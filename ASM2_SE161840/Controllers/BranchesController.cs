﻿using ASM2_SE161840.Data;
using ASM2_SE161840.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ASM2_SE161840.Controllers
{
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class BranchesController : ControllerBase
	{
		private readonly MyDBContext _dbContext;
		
		public BranchesController(MyDBContext dbContext)
		{
			_dbContext = dbContext;
		}
		[HttpGet]
		public IActionResult GetAllBranches() 
		{
			var branches = _dbContext.Branches.ToList();
			return Ok(branches);
		}
		[HttpGet("{id}")]
		public IActionResult GetBranchId(int id) 
		{
			var branch = _dbContext.Branches.SingleOrDefault(b => b.BranchId.Equals(id));
			if (branch != null)
			{
				return Ok(branch);
			}
			else 
			{
				return NotFound();
			}
		}
		[HttpPost]
		public IActionResult CreateBranch(BranchModel branchModel) 
		{
			try
			{
				var branch = new BranchData
				{
					BranchName = branchModel.BranchName,
					Address = branchModel.Address,
					City = branchModel.City,
					State = branchModel.State,
					ZipCode = branchModel.ZipCode,
				};
				_dbContext.Add(branch);
				_dbContext.SaveChanges();
				return Ok();
			}
			catch
			{
				return BadRequest();
			}
		}
		[HttpPut("{id}")]
		public IActionResult UpdateBranch(int id, BranchModel branchModel)
		{
			var brach = _dbContext.Branches.SingleOrDefault(t => t.BranchId == id);
			if (brach != null)
			{
				brach.BranchName = branchModel.BranchName;
				brach.Address = branchModel.Address;
				brach.City = branchModel.City;
				brach.State = branchModel.State;
				branchModel.ZipCode = branchModel.ZipCode;
				_dbContext.SaveChanges();
				return NoContent();
			}
			else
			{
				return NotFound();
			}
		}
		[HttpDelete("{id}")]
		public IActionResult DeleteBranch(int id, BranchModel branchModel)
		{
			try
			{


				var branch = _dbContext.Branches.SingleOrDefault(t => t.BranchId == id);
				if (branch != null)
				{
					_dbContext.Remove(branch);
					_dbContext.SaveChanges();
				}
				else
				{
					return NotFound();
				}
				return Ok();

			}
			catch { return BadRequest(); }
		}
	}
}
