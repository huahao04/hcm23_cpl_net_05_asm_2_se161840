﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASM2_SE161840.Data
{
	[Table("Branch")]
	public class BranchData
	{
		[Required, Key]
		public int BranchId { get; set; }
		[Required, MaxLength(100)]
		public string BranchName { get; set; }
		[Required, MaxLength(100)]
		public string Address { get; set; }
		[Required, MaxLength(100)]
		public string City { get; set; }
		[Required, MaxLength(100)]
		public string State { get; set; }
		[Required, MaxLength(100)]
		public string ZipCode { get; set; }
		public BranchData() { }
	}
}
