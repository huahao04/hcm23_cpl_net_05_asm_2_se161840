﻿using System.Collections.Generic;
using System;
using Microsoft.EntityFrameworkCore;

namespace ASM2_SE161840.Data
{
	public class MyDBContext : DbContext
	{
		public MyDBContext(DbContextOptions options) : base(options) { }

		#region DbSet
		public DbSet<BranchData> Branches { get; set; }
		#endregion
	}
}
