﻿namespace ASM2_SE161840.Model
{
	public class BranchModel
	{
	
		public string BranchName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
	}
}
