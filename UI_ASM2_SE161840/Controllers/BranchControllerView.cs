﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using UI_ASM2_SE161840.Models;

namespace UI_ASM2_SE161840.Controllers
{
	public class BranchControllerView : Controller
	{
		Uri baseAddress = new Uri("https://localhost:44343/api");
		private readonly HttpClient httpClient;
		public BranchControllerView()
		{
			httpClient = new HttpClient();
			httpClient.BaseAddress = baseAddress;
	    }
		[HttpGet]
		public IActionResult Index()
		{
			List<BranchViewModel> list = new();
			HttpResponseMessage response = httpClient.GetAsync(httpClient.BaseAddress + "/branch").Result;
			if (response.IsSuccessStatusCode)
			{
				string data = response.Content.ReadAsStringAsync().Result;
				list = JsonConvert.DeserializeObject<List<BranchViewModel>>(data);
			}
			return View(list);
		}
	}
}
