﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UI_ASM2_SE161840.Models
{
	public class BranchViewModel
	{
		[Required, Key]
		public int BranchId { get; set; }
		[Required, MaxLength(100), DisplayName("Name")]
		public string BranchName { get; set; }
		[Required, MaxLength(100)]
		public string Address { get; set; }
		[Required, MaxLength(100)]
		public string City { get; set; }
		[Required, MaxLength(100)]
		public string State { get; set; }
		[Required, MaxLength(100)]
		public string ZipCode { get; set; }
		
	}
}
